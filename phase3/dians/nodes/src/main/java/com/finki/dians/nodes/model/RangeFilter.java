package com.finki.dians.nodes.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RangeFilter {

    public Double lat = null;

    public Double lng = null;

    public Double radius = 0.002;
}
