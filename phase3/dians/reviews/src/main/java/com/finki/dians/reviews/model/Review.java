package com.finki.dians.reviews.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Document(collection = "reviews")
public class Review {

    @Id
    public String id;

    @NonNull
    public String nodeId;

    @NonNull
    public String userId;

    @NonNull
    public String content;

    @NonNull
    public Long timestamp;

    @NonNull
    public Integer rating;

}
