package com.finki.dians.users.payload.resources;

import com.finki.dians.users.model.ERole;
import com.finki.dians.users.model.Role;
import com.finki.dians.users.model.User;
import com.finki.dians.users.repository.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
@AllArgsConstructor
public class UserResourceAssembler implements ResourceAssemblerSupport<User, UserResource> {

    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public User fromResource(UserResource resource) {

        Set<Role> roles = this.setRoles(resource);

        return User.builder()
                .username(resource.getUsername())
                .email(resource.getEmail())
                .password(bCryptPasswordEncoder.encode(resource.getPassword()))
                .roles(roles)
                .build();
    }

    @Override
    public User fromResource(User oldEntity, UserResource resource) {
        Set<Role> roles = this.setRoles(resource);

        oldEntity.setRoles(roles);
        oldEntity.setEmail(resource.getEmail());
        oldEntity.setUsername(resource.getUsername());
        oldEntity.setPassword(bCryptPasswordEncoder.encode(resource.getPassword()));
        return oldEntity;


    }

    private Set<Role> setRoles(UserResource resource) {

        Set<Role> roles = new HashSet<>();

        if (resource.getRole().equalsIgnoreCase("admin")) {
            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);
        } else {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
        return roles;
    }


}
